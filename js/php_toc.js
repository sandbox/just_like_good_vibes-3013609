(function ($, Drupal) {
    "use strict";
    Drupal.behaviors.php_toc = {
        attach: function (context, settings) {
            $('.toc', context).once('php_toc').each(
                function () {
                    // Apply the myCustomBehaviour effect to the elements only once.
                    var $toc = $(this);
                    var $toc_list = $('.toc-list', $toc);
                    $(".toc-toggle", $toc).click(
                        function () {
                            $toc_list.toggle();
                            if($toc_list.is(":visible")) {
                                $(this).text(Drupal.t('Hide'));
                            } else {
                                $(this).text(Drupal.t('Show'));
                            }
                        }
                    );
                }
            );
        }
    };
})(jQuery, Drupal);

<?php

namespace Drupal\php_toc\Plugin\Filter;

use Drupal\php_toc\PhpTocGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to add ids to html tags h1,... , h6
 *
 * @Filter(
 *   id = "php_toc",
 *   title = @Translation("TOC : add id to html tags from h1 to h6"),
 *   description = @Translation("usefull to have anchors for table of contents generation. Add id to html tags from h1 to h6"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class PhpTocFilter extends FilterBase implements ContainerFactoryPluginInterface
{
    /**
     * PhpTocGeneratorInterface
     *
     * @var \Drupal\php_toc\PhpTocGeneratorInterface
     */
    protected $phpTocGenetator;

    /**
     * Constructs a \Drupal\editor\Plugin\Filter\EditorFileReference object.
     *
     * @param array                                    $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string                                   $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed                                    $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\php_toc\PhpTocGeneratorInterface $phpTocGenetator
     *   An entity manager object.
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, PhpTocGeneratorInterface $phpTocGenetator)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->phpTocGenetator = $phpTocGenetator;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('php_toc.generator')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function process($text, $langcode)
    {
        //\Drupal::logger("php_toc")->info("filter process addAnchors");
        return new FilterProcessResult($this->phpTocGenetator->addAnchors($text));
    }
}

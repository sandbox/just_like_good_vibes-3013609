<?php

namespace Drupal\php_toc\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;
use Drupal\php_toc\PhpTocGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'php_toc_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "php_toc_field_formatter",
 *   label = @Translation("Php toc field formatter"),
 *   field_types = {
 *     "text_long",
 *     "text",
 *     "text_with_summary"
 *   }
 * )
 */
class PhpTocFieldFormatter extends TextDefaultFormatter implements ContainerFactoryPluginInterface
{

    /**
     * PhpTocGeneratorInterface
     *
     * @var \Drupal\php_toc\PhpTocGeneratorInterface
     */
    protected $phpTocGenetator;

    /**
     * Constructs a FormatterBase object.
     *
     * @param string                                      $plugin_id
     *   The plugin_id for the formatter.
     * @param mixed                                       $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
     *   The definition of the field to which the formatter is associated.
     * @param array                                       $settings
     *   The formatter settings.
     * @param string                                      $label
     *   The formatter label display setting.
     * @param string                                      $view_mode
     *   The view mode.
     * @param array                                       $third_party_settings
     *   Any third party settings.
     * @param \Drupal\php_toc\PhpTocGeneratorInterface    $phpTocGenetator
     *   The toc generator service
     */
    public function __construct(
        $plugin_id,
        $plugin_definition,
        FieldDefinitionInterface $field_definition,
        array $settings,
        $label,
        $view_mode,
        array $third_party_settings,
        PhpTocGeneratorInterface $phpTocGenetator
    ) {
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
        $this->phpTocGenetator = $phpTocGenetator;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('php_toc.generator')
        );
    }


    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        $default_settings = [];
        return $default_settings + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $custom_form = [];
        return $custom_form + parent::settingsForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];
        // Implement settings summary.

        return $summary;
    }



    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        /*
        foreach ($items as $delta => $item) {
        $elements[$delta] = ['#markup' => $this->viewValue($item)];
        }
        */

        $elements = [];
        $elements_default = parent::viewElements($items, $langcode);

        // The ProcessedText element already handles cache context & tag bubbling.
        // @see \Drupal\filter\Element\ProcessedText::preRenderText()
        foreach ($items as $delta => $item) {
            $text_value = $elements_default[$delta]["#text"];
            $elements_default[$delta]["#text"] = $this->phpTocGenetator->addAnchors($text_value);
            $toc = $this->phpTocGenetator->generateToc($elements_default[$delta]["#text"]);

            $elements[$delta] = [
              "toc" => $toc,
              "content" => $elements_default[$delta]

            ];
        }

        return $elements;
    }
}

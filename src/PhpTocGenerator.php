<?php

namespace Drupal\php_toc;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Transliteration\TransliterationInterface;

/**
 * Class PhpTocGenerator.
 */
class PhpTocGenerator implements PhpTocGeneratorInterface
{

    /**
     * \Drupal\Core\Config\ConfigFactoryInterface definition.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;


    /**
     *
     * @var \Drupal\Component\Transliteration\TransliterationInterface
     */
    protected $transliteration;
    /**
     * Constructs a new PhpTocGenerator object.
     */
    public function __construct(ConfigFactoryInterface $config_factory, TransliterationInterface $transliteration)
    {
        $this->configFactory = $config_factory;
        $this->transliteration = $transliteration;
    }

    protected function getHtmlTags()
    {
        return [
            "h1" => 0,
            "h2" => 1,
            "h3" => 2,
            "h4" => 3,
            "h5" => 4,
            "h6" => 5
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function addAnchors($text)
    {
        //\Drupal::logger("debug")->info("Add anchors");
        $document = Html::load($text);
        $tags = $this->getHtmlTags();
        $all_elements = [];
        $ids = [];
        foreach ($tags as $htmltag => $depth) {
            $elements = $document->getElementsByTagName($htmltag);
            foreach ($elements as $tag_element) {
                /**
 * @var \DOMElement $tag_element
*/
                $existing_id = trim("".$tag_element->getAttribute('id'));
                if (strlen($existing_id) > 0 && array_key_exists($existing_id, $ids)) {
                    $existing_id = null;
                }
                if ($existing_id) {
                    $ids[$existing_id] = $existing_id;
                } else {
                    $unique_id = Html::getUniqueId($this->transliteration->transliterate($tag_element->textContent));
                    $tag_element->setAttribute("id", $unique_id);
                }
            }
        }
        return Html::serialize($document);
    }

    protected function flatTocToTree(&$flat_list)
    {
        $output = [
            '#children' => [],
            '#theme' => 'php_toc_container',
            '#attached' => [
                "library" => [
                    "php_toc/php_toc"
                ]
            ]
        ];
        $id_to_index_in_container = [];
        $current_position = [];
        foreach ($flat_list as $item) {
            $itempos = $item["#position"];
            if ($itempos+1 > count($current_position)) {
                for ($ii =count($current_position); $ii < $itempos; $ii++) {
                    $current_position[]="";
                }
                $this->treeAppendChildren($output, $item, $current_position, $id_to_index_in_container);
                $current_position[] = $item["#id"];
            } else {
                while ($itempos+1 <= count($current_position)) {
                    array_pop($current_position);
                }
                while (count($current_position)> 0 && $current_position[count($current_position)-1] == "") {
                    array_pop($current_position);
                }
                for ($ii =count($current_position); $ii < $itempos; $ii++) {
                    $current_position[]="";
                }
                $this->treeAppendChildren($output, $item, $current_position, $id_to_index_in_container);
                $current_position[] = $item["#id"];
            }
        }
        return $output;
    }
    private function treeAppendChildren(&$output, &$item, &$position, &$id_to_index_in_container)
    {
        $container = &$output['#children'];
        /*

         \Drupal::logger("debug")->info(
            "treeAppendChildren, item <pre>".print_r($item,TRUE)."</pre>".
            ",<br> position=<pre>".print_r($position,TRUE)."</pre>".
            ",<br> id_to_index_in_container=<pre>".print_r($id_to_index_in_container,TRUE)."</pre>".
            ",<br> output=<pre>".print_r($output,TRUE)."</pre>"

        );
        */
        $nlevels = 0;
        foreach ($position as $parent) {
            if (!$parent) {
                continue;
            }
            $container = &$container[$id_to_index_in_container[$parent]]["#children"];
            $nlevels ++;
        }
        $container[] = [
            "#theme" => 'php_toc_item',
            "#text" => $item["#text"],
            "#id" => $item["#id"],
            "#position" => $nlevels,
            "#real_position" => count($position),
            "#children" => []
        ];
        $id_to_index_in_container[$item["#id"]] = count($container) - 1;
    }



    protected function generateFlatTocRecursive(&$output, &$tags, $dom_node)
    {
        if (!isset($dom_node->childNodes)) {
            return;
        }
        foreach ($dom_node->childNodes as $childNode) {
            if (isset($childNode->tagName) && array_key_exists($childNode->tagName, $tags)) {
                $pos = $tags[$childNode->tagName];
                $content = $childNode->textContent;
                $id = $childNode->getAttribute("id");
                $output[] = [
                    "#position" => $pos,
                    "#id" => $id,
                    "#text" => $content,
                ];
            }
            $this->generateFlatTocRecursive($output, $tags, $childNode);
        }
    }

    public function generateToc($text)
    {
        $document = Html::load($text);
        $start = $document->documentElement;
        $flat_list = [];
        $tags = $this->getHtmlTags();
        $current_position = [];
        $this->generateFlatTocRecursive($flat_list, $tags, $start, $current_position);
        //\Drupal::logger("debug")->info("flat toc=<pre>".print_r($flat_list,TRUE)."</pre>");
        $tocTree = $this->flatTocToTree($flat_list);
        //\Drupal::logger("debug")->info("tree=<pre>".print_r($tocTree,TRUE)."</pre>");
        return $tocTree;
    }
}

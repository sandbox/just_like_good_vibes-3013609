<?php

namespace Drupal\php_toc\TwigExtension;

class PhpTocTwigExtension extends \Twig_Extension
{

    /**
     * Generates a list of all Twig filters that this extension defines.
     *
     * @return  array
     *   A key/value array that defines custom Twig filters. The key denotes the
     *   filter name used in the tag, e.g.:
     * @code
     *   {{ foo|testfilter }}
     * @endcode
     *
     *   The value is a standard PHP callback that defines what the filter does.
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('php_toc', '\Drupal\php_toc\TwigExtension\PhpTocTwigExtension::generateToc', ['is_safe' => array('all')])
            /*
            'php_toc_prepend' => new \Twig_Filter_Function(
                [
                    'Drupal\php_toc\TwigExtension\PhpTocTwigExtension',
                    'generateToc',
                    ['is_safe' => array('all')]
                ])
            */
        ];
    }

    /**
     * Gets a unique identifier for this Twig extension.
     *
     * @return string
     *   A unique identifier for this Twig extension.
     */
    public function getName()
    {
        return 'php_toc.twig_extension';
    }

    /**
     * generate a table of contents
     *
     * @param string $string
     *   The string to be filtered.
     *
     * @return string
     *   The filtered string.
     *
     * @see \Drupal\php_toc\PhpTocGenerator::generateToc()
     */
    public static function generateToc($input_tree)
    {
        $renderer = \Drupal::service("renderer");
        if (is_array($input_tree)) {
            $string = $renderer->renderRoot($input_tree);
        } else {
            $string = "".$input_tree;
        }
        $toc = \Drupal::service("php_toc.generator")->generateToc($string);
        return $toc;
        /* $toc_rendered = $renderer->renderRoot($toc);
        return $toc_rendered.$string;
        */
    }
}

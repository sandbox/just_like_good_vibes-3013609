<?php

namespace Drupal\php_toc;

/**
 * Interface PhpTocGeneratorInterface.
 */
interface PhpTocGeneratorInterface
{
    public function addAnchors($text);

    public function generateToc($text);
}
